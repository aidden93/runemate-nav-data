package com.runemate.web.transport.fixed;

import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class SpiritTreeTransport extends FixedTransport {

    @NonNull String location;

}
