package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.fixed.*;
import java.util.*;
import lombok.*;

@Getter
@RequiredArgsConstructor
public enum MushTree {

    HOUSE_ON_THE_HILL(
        "House on the Hill",
        new Coordinate(3764, 3880, 1),
        Requirements.none()
    ),
    VERDANT_VALLEY(
        "Verdant Valley",
        new Coordinate(3758, 3756),
        Requirements.none()
    ),
    STICKY_SWAMP(
        "Sticky Swamp",
        new Coordinate(3677, 3755),
        Requirements.none()
    ),
    MUSHROOM_MEADOW(
        "Mushroom Meadow",
        new Coordinate(3676, 3871),
        Requirements.none()
    );

    private final String location;
    private final Coordinate position;
    private final Requirement requirement;

    public static List<MushTreeTransport> transports() {
        final var results = new ArrayList<MushTreeTransport>();
        for (final var source : values()) {
            for (final var destination : values()) {
                if (source.equals(destination)) {
                    continue;
                }
                results.add(MushTreeTransport.builder()
                    .source(source.getPosition())
                    .destination(destination.getPosition())
                    .location(destination.getLocation())
                    .requirement(destination.getRequirement())
                    .build());
            }
        }
        return results;
    }
}
