package com.runemate.web.edge;

import java.util.*;

public abstract class EdgeBase<V> implements Edge<V> {

    @Override
    public /*final*/ boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Edge)) {
            return false;
        }
        Edge<?> that = (Edge<?>) o;
        return Float.compare(that.cost(), cost()) == 0 && origin().equals(that.origin()) &&
            destination().equals(that.destination()) && requirement().equals(that.requirement());
    }

    @Override
    public /*final*/ int hashCode() {
        return Objects.hash(origin(), destination(), cost(), requirement());
    }

    @Override
    public /*final*/ String toString() {
        return new StringJoiner(", ", Edge.class.getSimpleName() + "[", "]")
            .add("origin=" + origin())
            .add("destination=" + destination())
            .add("cost=" + cost())
            .add("requirement=" + requirement())
            .toString();
    }

    @Override
    public int type() {
        return Type.LITERAL;
    }
}
