package com.runemate.web.requirement;

import com.runemate.web.*;
import com.runemate.web.data.*;
import lombok.*;

@Value
public class RuneRequirement implements Requirement {

    @NonNull Rune rune;
    int quantity;

    @Override
    public boolean satisfy(@NonNull final WebContext context) {
        return context.hasRunes(rune, quantity);
    }

    @Override
    public int type() {
        return Type.RUNE;
    }
}
