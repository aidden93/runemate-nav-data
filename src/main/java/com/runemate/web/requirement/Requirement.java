package com.runemate.web.requirement;

import com.runemate.web.*;
import java.util.*;
import lombok.*;

@FunctionalInterface
public interface Requirement {

    boolean satisfy(@NonNull WebContext context);

    default int type() {
        return Type.UNKNOWN;
    }

    default Requirement and(@NonNull Requirement other) {
        if (this instanceof AndRequirement) {
            ((AndRequirement) this).getRequirements().add(other);
            return this;
        }
        final var reqs = new HashSet<Requirement>(2);
        Collections.addAll(reqs, this, other);
        return new AndRequirement(reqs);
    }

    default Requirement or(@NonNull Requirement other) {
        if (this instanceof OrRequirement) {
            ((OrRequirement) this).getRequirements().add(other);
            return this;
        }
        final var reqs = new HashSet<Requirement>(2);
        Collections.addAll(reqs, this, other);
        return new OrRequirement(reqs);
    }

    default Requirement not() {
        return new NotRequirement(this);
    }

    interface Type {

        int UNKNOWN = -1;
        int NONE = 0;
        int AND = 1;
        int OR = 2;
        int SKILL = 3;
        int ITEM = 4;
        int MEMBERS = 5;
        int QUEST_POINTS = 6;
        int QUEST_STATE = 7;
        int VARP = 8;
        int VARBIT = 9;
        int GOLD = 10;
        int EQUIPMENT = 11;
        int NOT = 12;
        int WILDERNESS = 13;
        int DIARY = 14;
        int RUNE = 15;
        int SPELLBOOK = 16;
        int DAILY_LIMIT = 17;
        int TELEPORTS_ENABLED = 18;
    }
}
