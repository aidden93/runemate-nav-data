package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class SpellbookRequirement implements Requirement {

    String spellbook;

    @Override
    public boolean satisfy(@NonNull final WebContext context) {
        return context.isOnSpellbook(spellbook);
    }

    @Override
    public int type() {
        return Type.SPELLBOOK;
    }

    public static SpellbookRequirement standard() {
        return new SpellbookRequirement("STANDARD");
    }

    public static SpellbookRequirement lunar() {
        return new SpellbookRequirement("LUNAR");
    }

    public static SpellbookRequirement ancient() {
        return new SpellbookRequirement("ANCIENT");
    }

    public static SpellbookRequirement arceuus() {
        return new SpellbookRequirement("ARCEUUS");
    }
}
