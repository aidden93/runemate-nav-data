package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class DiaryRequirement implements Requirement {

    Region region;
    Difficulty difficulty;

    @Override
    public boolean satisfy(@NonNull final WebContext context) {
        return context.isDiaryCompleted(region, difficulty);
    }

    @Override
    public int type() {
        return Type.DIARY;
    }

    public enum Region {
        ARDOUGNE,
        DESERT,
        FALADOR,
        FREMENNIK,
        KANDARIN,
        KARAMJA,
        KOUREND_AND_KEBOS,
        LUMBRIDGE,
        MORYTANIA,
        VARROCK,
        WESTERN_PROVINCES,
        WILDERNESS
    }

    public enum Difficulty {
        EASY,
        MEDIUM,
        HARD,
        ELITE
    }
}
