package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class MembersRequirement implements Requirement {

    boolean required;

    @Override
    public boolean satisfy(@NonNull final WebContext context) {
        return !required || context.isMember();
    }

    @Override
    public int type() {
        return Type.MEMBERS;
    }

}
