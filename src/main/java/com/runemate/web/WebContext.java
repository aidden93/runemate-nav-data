package com.runemate.web;

import com.runemate.web.data.*;
import com.runemate.web.requirement.*;
import com.runemate.web.util.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import lombok.*;

@Value
@Builder(toBuilder = true)
public class WebContext {

    @Builder.Default Map<String, Integer> inventory = Collections.emptyMap();
    @Builder.Default List<String> equipment = Collections.emptyList();
    @Builder.Default Map<SkillRequirement.Skill, Integer> skills = Collections.emptyMap();
    @Builder.Default @ToString.Exclude Map<Integer, Integer> varps = Collections.emptyMap();
    @Builder.Default @ToString.Exclude Map<Integer, Integer> varbits = Collections.emptyMap();
    @Builder.Default Map<String, QuestRequirement.State> quests = Collections.emptyMap();
    @Builder.Default Map<DiaryRequirement.Region, Map<DiaryRequirement.Difficulty, Boolean>> diaries = Collections.emptyMap();
    @Builder.Default Map<Rune, Integer> runes = Collections.emptyMap();
    @Builder.Default String spellbook = "UNKNOWN";
    @Builder.Default Map<DiaryRequirement.Region, Integer> dailyTeleportUses = Collections.emptyMap();
    @Builder.Default boolean allowingTeleports = true;

    int questPoints;
    boolean member;
    int gold;
    int wildernessLevel;

    public static WebContext empty() {
        return new WebContextBuilder().build();
    }

    public boolean hasQuestState(String quest, QuestRequirement.State state) {
        return state != QuestRequirement.State.UNKNOWN && quests.entrySet()
            .stream().anyMatch(e -> e.getKey().equalsIgnoreCase(quest) && Objects.equals(state, e.getValue()));
    }

    public boolean isDiaryCompleted(DiaryRequirement.Region region, DiaryRequirement.Difficulty difficulty) {
        Map<DiaryRequirement.Difficulty, Boolean> completions = diaries.get(region);
        return completions != null && completions.getOrDefault(difficulty, false);
    }

    public boolean hasInventoryItem(Pattern name, int quantity) {
        return inventory.entrySet()
            .stream().anyMatch(e -> name.matcher(e.getKey()).matches() && e.getValue() >= quantity);
    }

    public boolean isMember() {
        return member;
    }

    public boolean isWithinWildernessDepth(int min, int max) {
        return wildernessLevel >= min && wildernessLevel <= max;
    }

    public boolean isEquipped(Pattern name) {
        return equipment.stream().anyMatch(s -> name.matcher(s).matches());
    }

    public boolean hasSkillLevel(SkillRequirement.Skill skill, int level) {
        return skills.getOrDefault(skill, 0) >= level;
    }

    public boolean hasVarp(int varp, int value, BiFunction<Integer, Integer, Boolean> op) {
        return op.apply(varps.get(varp), value);
    }

    public boolean hasVarbit(int varbit, int value, BiFunction<Integer, Integer, Boolean> op) {
        return op.apply(varbits.get(varbit), value);
    }

    public boolean hasVarp(int varp, int value) {
        return hasVarp(varp, value, IntOp.EQ);
    }

    public boolean hasVarbit(int varbit, int value) {
        return hasVarbit(varbit, value, IntOp.EQ);
    }

    public boolean hasQuestPoints(int points) {
        return points <= questPoints;
    }

    public boolean hasGold(int quantity) {
        return quantity <= gold;
    }

    public boolean hasRunes(Rune rune, int quantity) {
        EnumSet<Rune> field;
        switch (rune) {
            case AIR:
                field = EnumSet.of(Rune.AIR, Rune.MIST, Rune.DUST, Rune.SMOKE);
                break;
            case EARTH:
                field = EnumSet.of(Rune.EARTH, Rune.MUD, Rune.DUST, Rune.LAVA);
                break;
            case WATER:
                field = EnumSet.of(Rune.WATER, Rune.MIST, Rune.MUD, Rune.STEAM);
                break;
            case FIRE:
                field = EnumSet.of(Rune.FIRE, Rune.LAVA, Rune.STEAM, Rune.SMOKE);
                break;
            default:
                field = EnumSet.of(rune);
        }
        int count = 0;
        for (var target : field) {
            final var held = runes.get(target);
            if (held != null) {
                count += held;
            }
        }
        return count >= quantity;
    }

    public boolean isOnSpellbook(String spellbook) {
        return this.spellbook.equals(spellbook);
    }

    public boolean hasRemainingUses(DiaryRequirement.Region region, int limit) {
        final var uses = dailyTeleportUses.get(region);
        return uses == null || uses < limit;
    }
}
